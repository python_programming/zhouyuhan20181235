#########################
# 1.aimed:python + pymysql 数据库操作
# 2.author:20181235周昱涵
# #########################
import pymysql

###
#1.创建数据库
###
conn = pymysql.connect(host='localhost', user='root', password='123456', charset='utf8mb4')
cur = conn.cursor()
# 创建数据库的sql(如果数据库存在就不创建，防止异常)
sql = "CREATE DATABASE IF NOT EXISTS games"
# 执行创建数据库的sql
cur.execute(sql)

###
#2.创建表
###
db = pymysql.connect(host="127.0.0.1",
                     user="root",
                     passwd="root",
                     db="games")
cur = db.cursor()
sql_2 = '''CREATE TABLE `players` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `account` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `money` INT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
'''
cur.execute(sql)

###
#3.插入
###
db = pymysql.connect(host="127.0.0.1",
                     user="root",
                     passwd="root",
                     db="games")
cur = db.cursor()
sql_insert = "insert into players (account,password,money) values('zhangsan','a123456',100);"
cur.execute(sql_insert)



###
#4.删除
###
db = pymysql.connect(host="127.0.0.1",
                     user="root",
                     passwd="root",
                     db="games")
cur = db.cursor()
sql_delete = "delete from players where account = 'zhangsan'"
cur.execute(sql_delete)

###
#5.更新
###
db = pymysql.connect(host="127.0.0.1",
                     user="root",
                     passwd="root",
                     db="games")
cur = db.cursor()
sql_update = "update players set money = 999 where account='zhangsan'"
cur.execute(sql_update)


###
#6.查询
###
db = pymysql.connect(host="127.0.0.1",
                     user="root",
                     passwd="root",
                     db="games")
cur = db.cursor()
sql_query = "select * from players;"
cur.execute(sql_insert)
result = cur.fetchall()
print(result)


cur.close()
db.close()