import socket
import os
import sys
import struct
import base64
# -*- coding:UTF-8 -*-
def socket_service_image():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('127.0.0.1', 6666))
        s.listen(10)
    except socket.error as msg:
        print(msg)
        sys.exit(1)

    print("Wait for Connection.....................")

    while True:
        sock, addr = s.accept()
        deal_image(sock, addr)


def deal_image(sock, addr):
    print("Accept connection from {0}".format(addr))

    while True:
        fileinfo_size = struct.calcsize('128sq')
        print('fileinfo_size is', fileinfo_size)
        buf = sock.recv(fileinfo_size)
        print('buf is ', buf)
        if buf:
            filename, filesize = struct.unpack('128sq', buf)
            print('filename ,filesize is', filename.decode(), filesize)
            fn = filename.decode().strip('\x00')
            print('fn is ', fn)
            new_filename = os.path.join('./','new_' + fn)

            recvd_size = 0
            fp = open(new_filename, 'wb')

            while not recvd_size == filesize:
                if filesize - recvd_size > 1024:
                    data = sock.recv(1024)
                    recvd_size += len(data)
                else:
                    data = sock.recv(1024)
                    recvd_size = filesize
                print(data)
                data=base64.b64decode(data)
                print('data is', data)
                fp.write(data)
            fp.close()
        sock.close()
        break


if __name__ == '__main__':
    socket_service_image()