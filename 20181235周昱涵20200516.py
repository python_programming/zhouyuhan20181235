import socket
import os
import sys
import struct
import base64
# -*- coding:UTF-8 -*-
def sock_client_image():
    while True:
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect(('127.0.0.1', 6666))

        except socket.error as msg:
            print(msg)
            print(sys.exit(1))
        filepath = input('input the file: ')
        fhead = struct.pack(b'128sq', bytes(os.path.basename(filepath), encoding='utf-8'),os.stat(filepath).st_size)
        s.send(fhead)

        fp = open(filepath, 'rb')
        while True:
            data = fp.read(1024)
            datq1 = base64.b64encode(data)
            if not data:
                print('{0} send over...'.format(filepath))
                break
            print(datq1)
            s.send(datq1)
        s.close()



if __name__ == '__main__':
    sock_client_image()